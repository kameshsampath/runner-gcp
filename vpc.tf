# VPC which will be used by the runner and Builder VMs
resource "google_compute_network" "runner_vpc" {
  name                    = "${var.vm_name}-vpc"
  auto_create_subnetworks = false
}

# Subnet used to allocate ips for all runner VMS
resource "google_compute_subnetwork" "runner_subnet" {
  name          = "${var.vm_name}-vpc-subnet"
  region        = var.gcp_region
  network       = google_compute_network.runner_vpc.name
  ip_cidr_range = "10.10.0.0/24"

  log_config {
    aggregation_interval = "INTERVAL_5_MIN"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}


#####################################################
## Firewall Rules
#####################################################

# Allow all egress from runner VPC
resource "google_compute_firewall" "runner_vpc_allow_egress" {
  name    = "allow-all-egress"
  network = google_compute_network.runner_vpc.name

  priority = 65535

  direction = "EGRESS"

  allow {
    protocol = "all"
  }

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }
}

# Deny all ingress to runner VPC
resource "google_compute_firewall" "runner_vpc_deny_ingress" {
  name    = "deny-all-ingress"
  network = google_compute_network.runner_vpc.name

  priority = 65535

  direction     = "INGRESS"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "all"
  }

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }
}

# Allow SSH, http, https to runner VM
# TODO use IAP Proxy
resource "google_compute_firewall" "runner_fw" {
  name     = "allow-ssh-9079"
  network  = google_compute_network.runner_vpc.name
  priority = 65534
  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["gitlab", "runner"]

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }
}
