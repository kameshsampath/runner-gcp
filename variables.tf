variable "gcp_project" {
  description = "project id"
}

variable "gcp_region" {
  description = "the region or zone where the cluster will be created."
  default     = "asia-south1"
}

variable "vm_name" {
  description = "The GitLab Runner VM name"
  default     = "gitlab-runner"
}

variable "vm_ssh_user" {
  description = "The SSH user for the VM. Defaults to the system user."
  type        = string
}

# gcloud compute machine-types list
variable "machine_type" {
  description = "the google cloud machine types for each cluster node."
  # https://cloud.google.com/compute/docs/general-purpose-machines#n2_machine_types
  default = "n2-standard-4"
}

variable "gitlab_registration_token" {
  description = "The token to use when registering the runner."
  type        = string
  sensitive   = true
}

variable "gitlab_registration_url" {
  description = "The URL to register the gitlab runner."
  type        = string
  default     = "https://gitlab.com/"
}

variable "gitlab_runner_image" {
  description = "The Docker Image used by the executor."
  type        = string
  default     = "alpine:latest"
}

