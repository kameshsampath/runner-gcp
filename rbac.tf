resource "google_service_account" "runner_sa" {
  account_id   = var.vm_name
  display_name = "The SA to run gitlab runner vm"
}