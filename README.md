# Deploy GitLab Self Managed Runner on GCP

Terraform automation scripts provision and configure a [Google Cloud Platform](https://cloud.google.com) VM infrastructure to run [GitLab Runner](https://docs.gitlab.com/runner/).

## Pre-requisites

- With a Service Account with roles
    - `Compute Engine Admin` - to create Google Compute Engine Instance
    - `Service Account` roles used to create/update/delete Service Account
      - iam.serviceAccounts.actAs
      - iam.serviceAccounts.get
      - iam.serviceAccounts.create
      - iam.serviceAccounts.delete
      - iam.serviceAccounts.update
      - iam.serviceAccounts.get
      - iam.serviceAccounts.getIamPolicy
      - iam.serviceAccounts.setIamPolicy
     Or simply you can add `Service Account Admin` and `Service Account User` roles
    - `Compute Network Admin`   - to create the VPC network to be used with GCE
- [Google Cloud SDK](https://cloud.google.com/sdk)
- [terraform](https://terraform.build)
- [Taskfile](https://taskfile.dev)

As we need to register a [GitLab Runner](https://docs.gitlab.com/runner) we need to get the GitLab Registration Token, check <https://docs.gitlab.com/runner/register/index.html#requirements> to find the right token.

### Optional tools

- [direnv](https://direnv.net)

## Download Sources

Clone the sources,

```shell
git clone https://gitlab.com/kameshsampath/runner-infra.git && cd "$(basename "$_" .git)"
export PROJECT_HOME="$PWD"
```

(OR) If you are using zsh, then you can use the following commands

```shell
take  https://gitlab.com/kameshsampath/runner-infra.git
export PROJECT_HOME="$PWD"
```

## Environment Setup

### Variables

When working with Google Cloud the following environment variables helps in setting the right Google Cloud context like Service Account Key file, project etc., You can use [direnv](https://direnv.net) or set the following variables on your shell,

```shell
export GOOGLE_APPLICATION_CREDENTIALS="the google cloud service account key json file to use"
export CLOUDSDK_ACTIVE_CONFIG_NAME="the google cloud cli profile to use"
export GOOGLE_CLOUD_PROJECT="the google cloud project to use"
```

You can find more information about gcloud cli configurations at <https://cloud.google.com/sdk/docs/configurations>.

As you may need to override few terraform variables that you don't want to check in to VCS, add them to a file called `.local.tfvars` and set the following environment variable to be picked up by terraform runs,

```shell
export TFVARS_FILE=.local.tfvars
```

>**NOTE**: All `*.local.tfvars` file are git ignored by this template.

Check the [Inputs](#inputs) section for all possible variables that are configurable.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | project id | `any` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | the region or zone where the cluster will be created. | `string` | `"asia-south1"` | no |
| <a name="input_gitlab_registration_token"></a> [gitlab\_registration\_token](#input\_gitlab\_registration\_token) | The token to use when registering the runner. | `string` | n/a | yes |
| <a name="input_gitlab_registration_url"></a> [gitlab\_registration\_url](#input\_gitlab\_registration\_url) | The URL to register the gitlab runner. | `string` | `"https://gitlab.com/"` | no |
| <a name="input_gitlab_runner_image"></a> [gitlab\_runner\_image](#input\_gitlab\_runner\_image) | The Docker Image used by the executor. | `string` | `"alpine:latest"` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | the google cloud machine types for each cluster node. | `string` | `"n2-standard-4"` | no |
| <a name="input_vm_name"></a> [vm\_name](#input\_vm\_name) | The GitLab Runner VM name | `string` | `"gitlab-runner"` | no |
| <a name="input_vm_ssh_user"></a> [vm\_ssh\_user](#input\_vm\_ssh\_user) | The SSH user for the VM. Defaults to the system user. | `string` | n/a | yes |

### Example
  
An example `.local.tfvars` that will use a Google Cloud project **my-awesome-project**, create Google Compute Engine VM named **gitlab-runner** in region **asia-south1**. The machine type of each cluster node will be **n2-standard-4**.

You may need to update following values with actuals from your GitLab Account,

- `gitlab_registration_token`

Following screenshot shows how to get a **project runner** `registration` token that could be used as value for `gitlab_registration_token`

![project runner token example](docs/images/gitlab_runner_token.png)

> **IMPORTANT**: Ensure the token used as `gitlab_registration_token` is at scope thats needed for you use case <https://docs.gitlab.com/ee/ci/runners/runners_scope.html>

```hcl
project_id                 = "my-awesome-project"
gitlab_registration_token  = "REPLACE WITH YOUR GitLab Registration Token"
```

> **NOTE**: For rest of the section we assume that your tfvars file is called `.local.tfvars`
>

## Tasks

The setup uses [Taskfile](https://taskfile.dev) for performing various tasks. All the available tasks can be listed using,

```shell
task --list  
```

Available tasks,

```shell
* clean:                     Clean all terraform artifacts/assets
* create_runner_infra:       Create the GitLab Runner Infra      (aliases: create, delegate, infra)
* destroy:                   Destroys the resources provisioned by Terraform
* format:                    Format terraform files
* init:                      Init terraform working directory
* readme:                    Build the README.md
* ssh:                       SSH into instance
* validate:                  Validate the terraform resources
```

## Create Environment

We will use terraform to create a GCE GitLab VM build infra,

```shell
task init
```
  
## Deploy GitLab Runner

Ensure you have the following values set in the `.local.tfvars` before running the `task apply` command.

```shell
task create
```

On a successful run of the scripts the runner gets registered with <http://gitlab.com>. An example project runner is as shown,

![Runner Registered](docs/images/gitlab_runner_connected.png)

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_project_id"></a> [project\_id](#output\_project\_id) | GCloud Project ID |
| <a name="output_region"></a> [region](#output\_region) | GCloud Region |
| <a name="output_runner_vm_name"></a> [runner\_vm\_name](#output\_runner\_vm\_name) | The VM name |
| <a name="output_vm_external_ip"></a> [vm\_external\_ip](#output\_vm\_external\_ip) | The external IP to access the VM |
| <a name="output_vm_ssh_user"></a> [vm\_ssh\_user](#output\_vm\_ssh\_user) | The SSH username to login into VM |
| <a name="output_zone"></a> [zone](#output\_zone) | VM Instance Zone |

## Cleanup

Run the following command to cleanup the infrastructure,

```shell
task destroy
```

## References

- [GitLab Runner](https://docs.gitlab.com/runner/)

## License

[Apache License](./../LICENSE)
