data "google_compute_zones" "available" {
  region = var.gcp_region
}

locals {
  # zone where runner will be provisioned
  google_zone = random_shuffle.az.result[0]
}