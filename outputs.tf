output "region" {
  value       = var.gcp_region
  description = "GCloud Region"
}

output "project_id" {
  value       = var.gcp_project
  description = "GCloud Project ID"
}

output "zone" {
  value       = local.google_zone
  description = "VM Instance Zone"
}

output "vm_ssh_user" {
  value       = var.vm_ssh_user
  description = "The SSH username to login into VM"
}

output "runner_vm_name" {
  value       = var.vm_name
  description = "The VM name"
}

output "vm_external_ip" {
  value       = google_compute_instance.runner_vm.network_interface.0.access_config.0.nat_ip
  description = "The external IP to access the VM"
}
