### Example
  
An example `.local.tfvars` that will use a Google Cloud project **my-awesome-project**, create Google Compute Engine VM named **gitlab-runner** in region **asia-south1**. The machine type of each cluster node will be **n2-standard-4**.

You may need to update following values with actuals from your GitLab Account,

- `gitlab_registration_token`

Following screenshot shows how to get a **project runner** `registration` token that could be used as value for `gitlab_registration_token`

![project runner token example](docs/images/gitlab_runner_token.png)

> **IMPORTANT**: Ensure the token used as `gitlab_registration_token` is at scope thats needed for you use case <https://docs.gitlab.com/ee/ci/runners/runners_scope.html>

```hcl
project_id                 = "my-awesome-project"
gitlab_registration_token  = "REPLACE WITH YOUR GitLab Registration Token"
```

> **NOTE**: For rest of the section we assume that your tfvars file is called `.local.tfvars`
>