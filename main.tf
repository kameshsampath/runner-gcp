resource "random_shuffle" "az" {
  input = data.google_compute_zones.available.names
}

resource "google_compute_instance" "runner_vm" {
  depends_on = [
    google_compute_network.runner_vpc,
    google_compute_subnetwork.runner_subnet
  ]

  name         = var.vm_name
  machine_type = var.machine_type
  zone         = local.google_zone

  tags = ["gitlab", "runner"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        "gitlab" = "runner"
      }
    }
  }

  // Local SSD disk
  scratch_disk {
    interface = "SCSI"
  }

  network_interface {
    # use the VPC runner subnet
    subnetwork = google_compute_subnetwork.runner_subnet.name

    access_config {
      // Ephemeral public IP
    }
  }

  metadata_startup_script = <<EOS
sudo apt-get update
sudo apt install net-tools
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

# Register the gitlab runner
sudo gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image "${var.gitlab_runner_image}" \
  --url "${var.gitlab_registration_url}" \
  --registration-token "${var.gitlab_registration_token}" \
  --description "docker-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "docker,gcp" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

EOS

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email = google_service_account.runner_sa.email
    # TODO trim down the scope to only what is needed
    scopes = ["cloud-platform"]
  }
}
